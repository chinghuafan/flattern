//
//  main.m
//  IntercomFlattern
//
//  Created by FAN CHING HUA on 2016/6/10.
//  Copyright © 2016年 footprint. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct LIST {
    struct LIST *next;  //pointer to next node in list
    char value;         //the value in node
    BOOL bIsList;       //set TRUE, when the subarray is started
    struct LIST *list;  //when the bIsList is setting TRUE, the list should be setting
} LIST_NODE;

int pos_count = 0;
char *end_char = NULL;
LIST_NODE *root = NULL;

LIST_NODE *flatten;


//to build the array structure by tree when the sub-array should be created.
//param :
//  location_ptr : current character
//  parent : parent node
void subarray_parser(char *location_ptr, LIST_NODE *parent) {
    if (location_ptr != NULL) {
        LIST_NODE *node = malloc(sizeof(LIST_NODE));
        memset(node, 0x00, sizeof(LIST_NODE));
        if (*location_ptr == '[') {
            if (parent == NULL) {
                node->bIsList = false;
                parent = node;
                root = parent;
            } else {
                LIST_NODE *next_node = malloc(sizeof(LIST_NODE));
                memset(next_node, 0x00, sizeof(LIST_NODE));
                next_node->bIsList = true;
                next_node->list = node;
                LIST_NODE *node_helper = parent;
                while (node_helper->next != NULL) {
                    node_helper = node_helper->next;
                }
                node_helper->next = next_node;
                parent = next_node->list;
            }
        } else {
            if (*location_ptr >= '0' && *location_ptr <= '9') {
                node->bIsList = false;
                node->value = *location_ptr;
                LIST_NODE *node_helper = parent;
                while (node_helper->next != NULL) {
                    node_helper = node_helper->next;
                }
                node_helper->next = node;
            }
            if (*location_ptr == ']') {
                end_char = location_ptr;
                return;
            }
        }
        
        subarray_parser(++location_ptr, parent);
        if ((end_char != NULL) && (*location_ptr == '[')) {
            long diff = (end_char - location_ptr);
            end_char = NULL;
            subarray_parser((location_ptr + diff) + 1, parent);
            
        }
    }
}

//to output the flat array from a nested array
//param :
//  from : input source nested array
//  to : distination flat element
void flatten_parser(LIST_NODE *from, LIST_NODE *to) {
    if (from == NULL) {
        return;
    }
    if (from->bIsList) {
        flatten_parser(from->list->next, to);
    }
    to->value = from->value;
    printf("%c ", from->value);
    if (from->next == NULL) {
        return;
    } else {
        LIST_NODE *next_dst = malloc(sizeof(LIST_NODE));
        memset(next_dst, 0x00, sizeof(LIST_NODE));
        to->next = next_dst;
        flatten_parser(from->next, to->next);
    }
    
}


//to remove the space from the user input, copy the string to distination buffer
//param :
//  src : input source string
//  dst : destnitation buffer
void removeSpaces(char *src, char *dst)
{
    int s_count, d_count=0;
    
    for (s_count = 0;src[s_count] != '\0';s_count++)
        if (src[s_count] != ' ') {
            dst[d_count] = src[s_count];
            d_count++;
        }
    dst[d_count] = '\0';
}

//to check input source is valid
//param :
//  src: input source string
//output :
//  when the string is valid, return true, else false
BOOL checkValidation(char *src) {
    int bResult = false;
    if (src == NULL)
        return bResult;
    int string_pos = 0;
    while (*(src + string_pos) != '\0') {
        if (((*(src + string_pos) >= '0') && (*(src + string_pos) <= '9')) ||
            ((*(src + string_pos) == '[') || (*(src + string_pos) == ']')) ||
            (*(src + string_pos) == ',')
            ) {
            bResult = true;
        } else {
            bResult = false;
            break;
        }
        string_pos++;
    }
    
    return bResult;
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        char string_list[100];
        char string_without_sp[100];
        
        memset(string_list, 0x00, sizeof(string_list));
        memset(string_without_sp, 0x00, sizeof(string_without_sp));
        printf("put array data here (ex:[1,[2,3],4]) : ");
        scanf("%[^\n]", string_list);
        removeSpaces(string_list, string_without_sp);
        if (!checkValidation(string_without_sp)) {
            printf("the string is invalid\r\n");
            return 0;
        }
        subarray_parser(string_without_sp, NULL);
        flatten = malloc(sizeof(LIST_NODE));
        memset(flatten, 0x00, sizeof(LIST_NODE));
        flatten_parser(root, flatten);
        
    }
    return 0;
}
